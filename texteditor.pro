TEMPLATE = app
TARGET = texteditor
QT += quick quickcontrols2

cross_compile: DEFINES += QT_EXTRA_FILE_SELECTOR=\\\"touch\\\"

HEADERS += \
    documenthandler.h

SOURCES += \
    texteditor.cpp \
    documenthandler.cpp

RESOURCES += \
    texteditor.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/quickcontrols2/texteditor
INSTALLS += target
