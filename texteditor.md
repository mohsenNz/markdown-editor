*Italic*    **Bold**
# Heading 1
## Heading 2

[Link](http://a.com)

* List
* List
* List

- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item

First Header              | Second Header
------------------------- | --------------------------
Content from cell 1       | Content from cell 2
Content in the 1th column | Content in the 2th column
