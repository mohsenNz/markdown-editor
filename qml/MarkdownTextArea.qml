import QtQuick          2.12
import QtQuick.Controls 2.12

TextArea {
  id: root

//  font.pixelSize: 20
  wrapMode: TextArea.Wrap
  focus: true
  selectByMouse: true
  persistentSelection: true
  background: Item { }

  signal clicked()

  // - - - functions - - -
  function toggleBlock(str1, str2 = str1, newline = false) {
    if (str1 === "") { return false }
    if (newline) {
      str1 = str1 + "\n"
      str2 = "\n" + str2
      if (text[selectionStart - 1] !== "\n" && selectionStart !== 0) {
        insert(selectionStart, "\n")
      }
    }

    var ssbs = selectionStart - str1.length   // Start of start block string
    var esbs = selectionStart - 1             // End of start block string
    var sebs = selectionEnd                   // Start of end block string
    var eebs = selectionEnd + str2.length - 1 // End of end block string

    if (text.slice(ssbs, esbs + 1) === str1 &&
        text.slice(sebs, eebs + 1) === str2) {
      remove(sebs, eebs + 1)
      remove(ssbs, esbs + 1)
    } else if (selectedText === "") {
      insert(cursorPosition, str1 + str2)
      cursorPosition -= str2.length
    } else {
      insert(selectionEnd, str2)
      insert(selectionStart, str1)
      select(selectionStart, selectionEnd - str2.length)
    }

    return true
  }

  function toggleLinesWidth(str) {
    for (var i = selectionEnd; i >= selectionStart; i--) {
      i = toggleLineWidth(str, i)
    }
  }

  function toggleLineWidth(str, curpos) {
    if (text[curpos] === "\n")
      curpos--

    while (text[curpos] !== "\n" && curpos > 0)
      curpos--

    curpos++
    if (text.slice(curpos, curpos + str.length) === str) {
      remove(curpos, curpos + str.length)
    } else {
      insert(curpos, str)
    }
    curpos--

    return curpos
  }

  function toggleBold() {
    toggleBlock("**")
    focus = true
  }

  function toggleItalic() {
    toggleBlock("*")
    focus = true
  }

  function toggleStrike() {
    toggleBlock("~~")
    focus = true
  }

  function toggleQuote() {
    toggleLinesWidth("> ")
    focus = true
  }

  function toggleOList() {
    toggleLinesWidth("* ")
    focus = true
  }

  function toggleBulletedList() {
    toggleLinesWidth("- ")
    focus = true
  }

  function toggleNumberedList() {
    toggleLinesWidth("1. ")
    focus = true
  }

  function toggleTaskList() {
    toggleLinesWidth("- [ ] ")
    focus = true
  }

  function toggleCodeblock() {
    toggleBlock("```", "```", true)
    focus = true
  }

  function insertLine() {
    insert(cursorPosition, "- - -")
    focus = true
  }

  function toggleLink() {
    toggleBlock("[", "]()")
    focus = true
  }

  function toggleHeader() {
    toggleLinesWidth("# ")
    focus = true
  }

  //______________________

  MouseArea {
    acceptedButtons: Qt.RightButton
    anchors.fill: parent
    hoverEnabled: true
    onClicked: root.clicked() //contextMenu.open()
    onContainsMouseChanged: {
      if (containsMouse) {
        cursorShape = Qt.IBeamCursor
      }
    }
  }
}

