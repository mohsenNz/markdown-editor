/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick          2.12
import QtQuick.Controls 2.12
import QtQuick.Window   2.12
import Qt.labs.platform 1.0   as Labs

import QtQuick.Controls.Suru2 2.4

import me.documenthandler 1.0


ApplicationWindow {
  id: window

  width: 1500
  height: 825
  minimumWidth: 1000
  minimumHeight: 256
  visible: true
  title: document.fileName + " - Text Editor Example"

  Component.onCompleted: {
    x = Screen.width / 2 - width / 2
    y = Screen.height / 2 - height / 2
  }

  onClosing: {
    if (document.modified) {
      quitDialog.open()
      close.accepted = false
    }
  }

  // Theme setting
  Suru2Conf {
    id: suru2Conf

//    Suru2.theme: Suru2.Dark
  }

  // Open
  Shortcut {
    id: openSc
    sequence: "Ctrl+o"//StandardKey.Open
    onActivated: openDialog.open()
  }
  // Save
  Shortcut {
    id: saveSc
    sequence: "Ctrl+s"//StandardKey.Save
    onActivated: {
      if (document.modified)
        document.save()
    }
  }
  // Save as
  Shortcut {
    id: saveasSc
    sequence: "Ctrl+Shift+s"//StandardKey.SaveAs
    onActivated: saveDialog.open()
  }
  // Quit
  Shortcut {
    id: quitSc
    sequence: "Ctrl+q"//StandardKey.Quit
    onActivated: close()
  }
  // Copy
  Shortcut {
    id: copySc
    sequence: "Ctrl+c"//StandardKey.Copy
    onActivated: iTextArea.copy()
  }
  // Cut
  Shortcut {
    id: cutSc
    sequence: "Ctrl+x"//StandardKey.Cut
    onActivated: iTextArea.cut()
  }
  // Paste
  Shortcut {
    id: pasteSc
    sequence: "Ctrl+v"//StandardKey.Paste
    onActivated: iTextArea.paste()
  }
  // Bold
  Shortcut {
    id: boldSc
    sequence: "Ctrl+b"//StandardKey.Bold
    onActivated: iTextArea.toggleBold()
  }
  // Italic
  Shortcut {
    id: italicSc
    sequence: "Ctrl+i"//StandardKey.Italic
    onActivated: iTextArea.toggleItalic()
  }
  // Zoom in
  Shortcut {
    id: zoomInSc
    sequence: "Ctrl++"//StandardKey.Italic
    onActivated: iTextArea.font.pointSize++
  }
  // Zoom out
  Shortcut {
    id: zoomOutSc
    sequence: "Ctrl+-"//StandardKey.Italic
    onActivated: iTextArea.font.pointSize--
  }
  // Zoom original
  Shortcut {
    id: zoomOrgSc
    sequence: "Ctrl+0"//StandardKey.Italic
    onActivated: iTextArea.font.pointSize = 14
  }
  // Underline
//  Shortcut {
//    sequence: StandardKey.Underline
//    onActivated: document.underline = !document.underline
//  }

  DocumentHandler {
    id: document

    document: iTextArea.textDocument
    cursorPosition: iTextArea.cursorPosition
    selectionStart: iTextArea.selectionStart
    selectionEnd: iTextArea.selectionEnd
    textColor: colorDialog.color
    defaultUrl: "qrc:/texteditor.md"

    Component.onCompleted: {
      if (Qt.application.arguments.length > 1) {
        document.load(currentDir + "/" + Qt.application.arguments[1])
      } else {
        document.load(defaultUrl)
      }
    }

    onLoaded: {
      iTextArea.text = text
    }

    onError: {
      errorDialog.text = message
      errorDialog.visible = true
    }
  }

  header: ToolBar {
    id: headerBar

    leftPadding: 8

    Row {
      id: leftRow
      width: parent.width

      Repeater {
        id: headerR

        model: [
          // File
          [{"name": "open",       "icon": Suru2.icons.document_open,    "tooltip": openSc.sequence},
           {"name": "save",       "icon": Suru2.icons.document_save,    "tooltip": saveSc.sequence},
           {"name": "save as",    "icon": Suru2.icons.document_save_as, "tooltip": saveasSc.sequence}],
          // Markdown text format
          [{"name": "bold",       "icon": Suru2.icons.format_text_bold, "tooltip": boldSc.sequence},
           {"name": "italic",     "icon": Suru2.icons.format_text_italic,"tooltip": italicSc.sequence},
//           {"name": "underline",  "icon": Suru2.icons.format_text_underline},
           {"name": "strike",     "icon": Suru2.icons.format_text_strikethrough}],
          // Markdown text-block foramt
          [{"name": "heading",    "text": "<b>H</b>"},
           {"name": "quote",      "icon": "qrc:/icons/quote_freepik.svg"},
           {"name": "codeblock",  "icon": "qrc:/icons/coding_Kiranshastry.svg"}],
          // Markdown list foramt
          [{"name": "bullList",   "icon": "qrc:/icons/list_Google.svg"},
           {"name": "numberList", "icon": "qrc:/icons/numberlist_Google.svg"},
           {"name": "taskList",   "icon": "qrc:/icons/checkbox_Those_icons.svg"}],
          // Markdown list foramt
          [{"name": "link",       "icon": Suru2.icons.insert_link},
           {"name": "line",       "text": "<b>-</b>"}]
        ]

        delegate: Row {
          height: headerBar.height

          Repeater {
            model: modelData
            anchors.verticalCenter: parent.verticalCenter

            delegate: ToolButton {
              anchors.verticalCenter: parent.verticalCenter
              text: modelData.text !== undefined ? modelData.text : ""
              icon.source: modelData.icon !== undefined ? modelData.icon : ""
              checkable: modelData.name === "alignLeft" || modelData.name === "alignRight"

              checked: {
                if (modelData.name === "alignLeft")
                  return document.alignment === Qt.AlignLeft
                else if (modelData.name === "alignRight")
                  return document.alignment === Qt.AlignRight
                else
                  return false
              }

              ToolTip.visible: hovered && modelData.tooltip !== undefined
              ToolTip.text: modelData.tooltip !== undefined ? modelData.tooltip : ""

              onClicked: {
                switch (modelData.name) {
                case "open":
                  openDialog.open()
                  break
                case "save":
                  if (document.modified) {
                    document.save()
                  }
                  iTextArea.focus = true

                  break
                case "save as":
                  saveDialog.open()
                  break
                case "bold":
                  iTextArea.toggleBold()
                  break
                case "italic":
                  iTextArea.toggleItalic()
                  break
                case "underline":
                  break
                case "strike":
                  iTextArea.toggleStrike()
                  break
                case "heading":
                  iTextArea.toggleHeader()
                  break
                case "quote":
                  iTextArea.toggleQuote()
                  break
                case "codeblock":
                  iTextArea.toggleCodeblock()
                  break
                case "bullList":
                  iTextArea.toggleBulletedList()
                  break
                case "numberList":
                  iTextArea.toggleNumberedList()
                  break
                case "taskList":
                  iTextArea.toggleTaskList()
                  break
                case "link":
                  iTextArea.toggleLink()
                  break
                case "line":
                  iTextArea.insertLine()
                  break
                case "font":
                  fontDialog.currentFont.family = iTextArea.font.family;
                  fontDialog.currentFont.pointSize = iTextArea.font.pointSize;
                  fontDialog.open();
                  break
                case "alignLeft":
                  document.alignment = Qt.AlignLeft
                  break
                case "alignRight":
                  document.alignment = Qt.AlignRight
                  break
                default:
                  console.assert(false, "Name error!")
                }

                iTextArea.focus = true
              }
            }
          }

          ToolSeparator { visible: index < headerR.model.length - 1 }
        }
      }
    }

    Row {
      id: rightRow

      anchors.right: parent.right

      height: headerBar.height

      // Font
      ToolButton {
        anchors.verticalCenter: parent.verticalCenter

        text: "<b>A</b>"


        onClicked: {
          fontDialog.currentFont.family = iTextArea.font.family;
          fontDialog.currentFont.pointSize = iTextArea.font.pointSize;
          fontDialog.open();
        }
      }

      // Zoom in
      ToolButton {
        anchors.verticalCenter: parent.verticalCenter
        ToolTip.visible: hovered
        ToolTip.text: zoomInSc.sequence

        icon.source: Suru2.icons.zoom_in

        onClicked: {
          iTextArea.font.pointSize++
        }
      }

      // Zoom out
      ToolButton {
        anchors.verticalCenter: parent.verticalCenter
        ToolTip.visible: hovered
        ToolTip.text: zoomOutSc.sequence
        icon.source: Suru2.icons.zoom_out

        onClicked: {
          iTextArea.font.pointSize--
        }
      }

      // Zoom original
      ToolButton {
        anchors.verticalCenter: parent.verticalCenter
        ToolTip.visible: hovered
        ToolTip.text: zoomOrgSc.sequence
        icon.source: Suru2.icons.zoom_original

        onClicked: {
          iTextArea.font.pointSize = 14
        }
      }

      // Toggle dark-light theme
      ToolButton {
        anchors.verticalCenter: parent.verticalCenter
        icon.source: suru2Conf.Suru2.theme === Suru2.Light ?
                       Suru2.icons.weather_clear_night : Suru2.icons.weather_clear

        onClicked: {
          if (suru2Conf.Suru2.theme === Suru2.Light) {
            suru2Conf.Suru2.theme = Suru2.Dark
          } else {
            suru2Conf.Suru2.theme = Suru2.Light
          }
        }
      }
    }
  }

  // Texts Container
  Row {
    anchors.fill: parent

    spacing: 0
    clip: true

    // Input text
    Rectangle {
      height: parent.height
      width: parent.width / 2
      color: suru2Conf.Suru2.theme === Suru2.Light ? suru2Conf.Suru2.backgroundColor : "#282828"

      Flickable {
        id: inputFlick

        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        anchors.fill: parent

        TextArea.flickable: MarkdownTextArea {
          id: iTextArea

          topPadding: 16
          leftPadding: 8
          rightPadding: 24
          bottomPadding: 8

          font.family: Suru2.units.fontCodeBlock.family
          font.pointSize: 14

          selectionColor: suru2Conf.Suru2.theme === Suru2.Light ? Suru2.accentColor : "#505050"

          onTextChanged: {
            inputFlick.newText = true
          }

          onClicked: {
            contextMenu.open()
          }
        }

        ScrollBar.vertical: ScrollBar {
          onPositionChanged: {
            if (pressed)
              outputFlick.contentY = inputFlick.contentY * oTextArea.height / iTextArea.height
          }
        }

        property bool newText: false

        onContentYChanged: {
          if (moving || newText) {
            outputFlick.contentY = contentY * oTextArea.height / iTextArea.height
          }

          newText = false
        }
      }
    }

    // Markdown view
    Rectangle {
      height: parent.height
      width: parent.width / 2
      color: suru2Conf.Suru2.theme === Suru2.Light ? "#d3d7cf" : "#414141"

      Flickable {
        id: outputFlick

        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        anchors.fill: parent

        contentHeight: Math.max(iTextArea.height, oTextArea.height) //+ 64

        ScrollBar.vertical: ScrollBar {
          id: sb
          onPositionChanged: {
            if (pressed) {
              inputFlick.contentY = outputFlick.contentY * iTextArea.height / oTextArea.height
            }
          }
        }

        TextArea {
          id: oTextArea

          width: parent.width

//          font.pixelSize: 20
          font.pointSize: 14
          textFormat: TextArea.MarkdownText
          wrapMode: TextArea.Wrap
          text: iTextArea.text
          readOnly: true
          topPadding: 16
          leftPadding: 8
          rightPadding: 24
          bottomPadding: 8

          background: Item {}

          onLinkActivated: Qt.openUrlExternally(link)
        }

        onContentYChanged: {
          if (moving) {
            inputFlick.contentY = contentY * iTextArea.height / oTextArea.height
          }

        }
      }
    }

  }

  Menu {
    id: contextMenu

    MenuItem {
      text: qsTr("Copy")
      enabled: iTextArea.selectedText
      onTriggered: iTextArea.copy()
    }
    MenuItem {
      text: qsTr("Cut")
      enabled: iTextArea.selectedText
      onTriggered: iTextArea.cut()
    }
    MenuItem {
      text: qsTr("Paste")
      enabled: iTextArea.canPaste
      onTriggered: iTextArea.paste()
    }

    MenuSeparator {}

    MenuItem {
      text: qsTr("Font...")
      onTriggered: fontDialog.open()
    }

    MenuItem {
      text: qsTr("Color...")
      onTriggered: colorDialog.open()
    }
  }

  Dialog {
    id: errorDialog

    property alias text: errorLabel.text

    modal: true
    title: qsTr("Error")

    x: (window.width - width) / 2
    y: (window.height - height) / 2

    Label { id: errorLabel }

    standardButtons: Dialog.Close
  }

  Dialog {
    id: quitDialog

    modal: true
    title: qsTr("Quit")
    focus: true

    x: (window.width - width) / 2
    y: (window.height - height) / 2

    Label {
      text: qsTr("The file has been modified. Quit anyway?")
      focus: true

      Keys.onReturnPressed: {
        if (quitDialog.opened)
          quitDialog.accept()
      }
    }

    standardButtons: Dialog.Ok | Dialog.Cancel

    onAccepted: Qt.quit()
  }

  Labs.FileDialog {
    id: openDialog
    fileMode: Labs.FileDialog.OpenFile
    selectedNameFilter.index: 1
    nameFilters: ["Text files (*.txt)", "Markdown file (*.md)", "HTML files (*.html *.htm)"]
    folder: Labs.StandardPaths.writableLocation(Labs.StandardPaths.DocumentsLocation)
    onAccepted: {
      document.load(file)
    }
  }

  Labs.FileDialog {
    id: saveDialog
    fileMode: Labs.FileDialog.SaveFile
    defaultSuffix: document.fileType
    nameFilters: openDialog.nameFilters
    selectedNameFilter.index: document.fileType === "txt" ? 0 : 1
    folder: Labs.StandardPaths.writableLocation(Labs.StandardPaths.DocumentsLocation)
    onAccepted: document.saveAs(file)
  }

  Labs.FontDialog {
    id: fontDialog

    currentFont: iTextArea.font

    onAccepted: {
      iTextArea.font.family = font.family;
      iTextArea.font.pointSize = font.pointSize;
    }
  }

  Labs.ColorDialog {
    id: colorDialog
    currentColor: "black"
  }

}
